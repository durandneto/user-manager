'use strict';

angular.module('app', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngResource',
    'ngStorage',
    'ngMap',
    'ui.router',
    'ui.bootstrap',
    'oc.lazyLoad',
    'pascalprecht.translate'
]);
