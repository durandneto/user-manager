'use strict';
 
(function(angular) {
 /**
 * Controller do seriados.
 */
angular.module('app').controller('UserSigninController', UserSigninController);
 
 UserSigninController.$inject = ['$scope',   '$state','UserService'];
 
 function UserSigninController($scope, $state, UserService) {
    $scope.user = {};
    $scope.authError = null;

    $scope.login = function() {
      UserService.login($scope.user,function(response){
        if (response.status == 'success'){
          $scope.app.user = response.data;
          $scope.authError = null;
          $state.go('app.home'); 
        }else{
          $scope.authError = response.message;
          $scope.authError += ' - Usuário ou senha inválidos.' ;
        }
      });
    };

 }
 
})(angular);