'use strict';
 
(function(angular) {
 /**
 * Controller do seriados.
 */
angular.module('app').controller('UserController', UserController);
 
 UserController.$inject = ['$scope', '$http', '$state','UserService'];
 
 function UserController($scope, $http, $state,UserService) {
    $scope.user = {};
    $scope.authError = null;

    $scope.login = function() {
      UserService.login($scope.user,function(response){
        if (response.status == 'success'){
          $scope.app.user = response.data;
          $scope.authError = null;
          $state.go('app.dashboard-v3'); 
        }else{
          $scope.authError = response.message;
          $scope.authError += ' - Usuário ou senha inválidos.' ;
        }
      });
    };

 }
 
})(angular);