app.factory('UserService', function($http,$timeout,$localStorage) {

	this.login = function(user,callback){
		$http.post($localStorage.api + '/user/login',user)
        .success(function (response) {
        	$timeout(function(){
	            callback(response);
        	});
        })
        .error(function(response){                    
            $timeout(function(){
	            callback(response);
        	});
        });
	}; 

    this.signup = function(user,callback){
        $http.post($localStorage.api + '/user',user)
        .success(function (response) {
            $timeout(function(){
                callback(response);
            });
        })
        .error(function(response){                    
            $timeout(function(){
                callback(response);
            });
        });
    };  

	return this;
}); 