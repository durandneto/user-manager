// lazyload config

angular.module('app')
    /**
   * jQuery plugin config use ui-jq directive , config the js and css files that required
   * key: function name of the jQuery plugin
   * value: array of the css js file located
   */
  .constant('JQ_CONFIG', {
    
      moment:         [   '../node_modules/moment/moment.js'],
      slider:         [   '../node_modules/bootstrap-slider/dist/bootstrap-slider.min.js',
                          '../node_modules/bootstrap-slider/dist/css/bootstrap-slider.css'],
    }
  )
  .constant('MODULE_CONFIG', [
       
      {
          name: 'toaster',
          files: [
              '../libs/angular/angularjs-toaster/toaster.js',
              '../libs/angular/angularjs-toaster/toaster.css'
          ]
      } 
    ]
  )
  // oclazyload config
  .config(['$ocLazyLoadProvider', 'MODULE_CONFIG', function($ocLazyLoadProvider, MODULE_CONFIG) {
      // We configure ocLazyLoad to use the lib script.js as the async loader
      $ocLazyLoadProvider.config({
          debug:  true,
          events: true,
          modules: MODULE_CONFIG
      });
  }])
;
