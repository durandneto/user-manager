'use strict';
/**
 * Config for the router
 */
angular.module('app')
  .run(
    [          '$rootScope', '$state', '$stateParams',
      function ($rootScope,   $state,   $stateParams) {
        console.log("rouetr");
          $rootScope.$state = $state;
          $rootScope.$stateParams = $stateParams;        
      }
    ]
  )
  .config(
    [          '$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 'MODULE_CONFIG', 
      function ($stateProvider,   $urlRouterProvider, JQ_CONFIG, MODULE_CONFIG) {

          var   layout = "/app/templates/site.html";
          var   layoutSignin = "/app/templates/signin.html";

          $urlRouterProvider
            .otherwise('/ubber');

          $stateProvider
              .state('site', {
                abstract : true
                , url: '/site'
                , templateUrl: layout 
              })
              .state('site.home', {
                url: '/home'
                , templateUrl: "app/modules/site/home.html" 
              })
              .state('site.user', {
                abstract : true
                , url: '/user'
                , templateUrl: "app/modules/user/views/user.html" 
              })
              .state('site.user.me', {
                url: '/me'
                , templateUrl: "app/modules/user/views/user.me.html" 
              })
              .state('site.user.signin', {
                  url: '/signin'
                  , templateUrl: "app/modules/user/views/user.signin.html" 
                  , controller : "UserSigninController"
                  , resolve: load([
                      'app/modules/user/controllers/UserSigninController.js'
                      , 'app/modules/user/services/UserService.js'
                      ])
              })

              .state('ubber', {
                url: '/ubber'
                , templateUrl: "app/modules/ubber/views/index.html" 
                , resolve: load([
                      'app/modules/ubber/controllers/UbberController.js'
                      ])
              })
              .state('ubber-show', {
                url: '/ubber-show'
                , templateUrl: "app/modules/ubber/views/show.html" 
                , resolve: load([
                      'app/modules/ubber/controllers/UbberController.js'
                      ])
              })
              .state('ubber-waiting', {
                url: '/ubber-waiting'
                , templateUrl : "app/modules/ubber/views/waiting.html" 
                , controller  : "UbberWaitingController"
                , resolve: load([
                      'app/modules/ubber/controllers/UbberController.js'
                      ])
              })
              .state('site.ubber-voucher', {
                url: '/ubber-voucher'
                , templateUrl : "app/modules/ubber/views/ubber-voucher.html" 
                , controller  : "UbberVoucherController"
                , resolve: load([
                      'app/modules/ubber/controllers/UbberController.js'
                      ])
              })

              ;

          function load(srcs, callback) {
            return {
                deps: ['$ocLazyLoad', '$q',
                  function( $ocLazyLoad, $q ){
                    var deferred = $q.defer();
                    var promise  = false;
                    srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
                    if(!promise){
                      promise = deferred.promise;
                    }
                    angular.forEach(srcs, function(src) {
                      console.log(src);
                      promise = promise.then( function(){
                        if(JQ_CONFIG[src]){
                          return $ocLazyLoad.load(JQ_CONFIG[src]);
                        }
                        angular.forEach(MODULE_CONFIG, function(module) {
                          if( module.name == src){
                            name = module.name;
                          }else{
                            name = src;
                          }
                        });
                        return $ocLazyLoad.load(name);
                      } );
                    });
                    deferred.resolve();
                    return callback ? promise.then(function(){ return callback(); }) : promise;
                }]
            }
          }


      }
    ]
  );
