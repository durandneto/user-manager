angular.module('app').directive('googlePlace', function() {
     return {
        link: function(scope, element, attrs) {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {}
            };
            console.log(google.maps);
            
            scope.gPlace = new google.maps.places.Autocomplete(element[0] || '', options);

            google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
                var geoComponents = scope.gPlace.getPlace();
                var latitude = geoComponents.geometry.location.lat();
                var longitude = geoComponents.geometry.location.lng();
                var addressComponents = geoComponents.address_components;

                addressComponents = addressComponents.filter(function(component){
                    switch (component.types[0]) {
                        case "locality": // city
                            return true;
                        case "administrative_area_level_1": // state
                            return true;
                        case "country": // country
                            return true;
                        default:
                            return false;
                    }
                }).map(function(obj) {
                    return obj.long_name;
                });

                // addressComponents.push(latitude, longitude);

                scope.$apply(function() {
                    scope.location = addressComponents[0] + ", "+addressComponents[1] + ", "+addressComponents[2]; 
                    scope.locationLongitude = longitude; 
                    scope.locationLatitude = latitude; 
                });
            });
        }
    };
})