'use strict';
 
 /**
 * MainController  .
 */
angular.module('app').controller('MainController', MainController);
 
 MainController.$inject = ['$scope', '$localStorage'];
 
 function MainController($scope, $localStorage) {
  // config
      $scope.app = {
        name: 'System Upload by Durand Neto'
        ,version: '1.0.21'
        // for chart colors
        , baseApi : 'http://reboquerecife.com.br/byereport/api/public/index.php'
         
        , environment : 'test'
        , color: {
            primary: '#458afe'
          , info:    '#23b7e5'
          , success: '#43b77a'
          , warning: '#fad733'
          , danger:  '#f06d54'
          , light:   '#e8eff0'
          , dark:    '#2c2d31'
          , black:   '#252525'
        } 
        , loading:false
        , user:{
        	"isGuest":true
        	, info:{}
       	}
        , settings:{
        	bannerHeader:true
        }
      };


      $scope.$watch('app.user', function(){
        $localStorage.user = $scope.app.user;
      }, true);

      if(!angular.isDefined($localStorage.user)){
        $localStorage.user = {"isGuest":true,info:{} };
        $scope.app.user = $localStorage.user;
        console.log($scope.app.user);
      }else{
        $scope.app.user = $localStorage.user;
      }

          // save settings to local storage
      if ( angular.isDefined($localStorage.settings) ) {
        $scope.app.settings = $localStorage.settings;
      } else {
        $localStorage.settings = $scope.app.settings;
      }

      $scope.$watch('app.settings', function(){
        // save to local storage
        $localStorage.settings = $scope.app.settings;
      }, true);

      $localStorage.api = $scope.app.baseApi; 


 };

 app.controller('CarouselDemoCtrl2', function ($scope) {
  $scope.myInterval = 5000;
  $scope.noWrapSlides = false;
  var slides = $scope.slides = [];
  $scope.addSlide = function() {
    console.log('asdas');
    var newWidth = 600 + slides.length + 1;
    slides.push({
      image: '//placekitten.com/' + newWidth + '/300',
      text: ['More','Extra','Lots of','Surplus'][slides.length % 4] + ' ' +
        ['Cats', 'Kittys', 'Felines', 'Cutes'][slides.length % 4]
    });
  };
  for (var i=0; i<4; i++) {
    $scope.addSlide();
  }
});

 app.controller('CarouselDemoCtrl', ['$scope', function($scope) {
    $scope.myInterval = 5000;
    var slides = $scope.slides = [];
    $scope.addSlide = function() {
      var newWidth = 900 + slides.length + 1;
      slides.push({
        image: 'http://placekitten.com/' + newWidth + '/300',
        text: ['Carousel text #0','Carousel text #1','Carousel text #2','Carousel text #3'][slides.length % 4]
      });
    };
    for (var i=0; i<4; i++) {
      $scope.addSlide();
    }
  }])
  ; 


 
